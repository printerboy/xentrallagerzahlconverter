#!/usr/bin/env python3
from xml.dom import minidom
import sys
import datetime
import time
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler


class ExampleHandler(FileSystemEventHandler):
    def on_created(self, event):  # when file is created
        # do something, eg. call your function to process the image
        newestFile = event.src_path.split('.')

        print('Neue Datei erkannt! ' + newestFile[1] + "." + newestFile[2])
        self.handleFile("./" + newestFile[1] + "." + newestFile[2])

    def handleFile(self, file):
        document = minidom.parse(file)

        artikelListe = document.getElementsByTagName('artikel_list')

        if (len(artikelListe) == 0):
            print('     Bei der Datei handelt es sich nicht um Lagerinformationen. Ich ignoriere die Datei!')

        date = datetime.datetime.now()
        outputName = str(date.year) + '-' + str(date.month) + '-' + str(date.day) + '-parsedStorage.xml'

        newFilePath = "./Lagerzahlen/" + outputName

        for list in artikelListe:
            artikel = list.getElementsByTagName('artikel')

            if (len(artikel) == 0):
                print(
                    '     Es handelt sich zwar um Lagerinformationen, es wurden aber keine Artikel in der Datei hinterlegt!')

            for art in artikel:
                nummer = art.getElementsByTagName('nummer')
                regalString = ''

                for n in nummer:
                    key = n.firstChild.data.split('GP1')
                    regalString += 'H' + key[1]

                regal = document.createElement('Regal')
                content = document.createTextNode(regalString)
                regal.appendChild(content)
                art.appendChild(regal)

                document.writexml(open(newFilePath, 'w+'))

        print("     Datei " + file + " verarbeitet! Du findest die Datei unter: " + newFilePath)


observer = Observer()
event_handler = ExampleHandler()  # create event handler
# set observer to use created handler in directory
observer.schedule(event_handler, path='./Import')
observer.start()

# sleep until keyboard interrupt, then stop + rejoin the observer
try:
    while True:
        time.sleep(1)
except KeyboardInterrupt:
    observer.stop()

observer.join()
